package net.ogd3.ting;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import net.ogd3.ting.containers.TingDevice;
import net.ogd3.ting.containers.TingPlan;
import net.ogd3.ting.containers.TingUsage;
import net.ogd3.ting.containers.TingUsageDetail;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ting {
    public static final String TAG = "Ting";

    public static final int TYPE_MINUTES = 0;
    public static final int TYPE_MESSAGES = 1;
    public static final int TYPE_MEGABYTES = 2;

    public static final int ERROR_NOT_ACTIVATED = 1;
    public static final int ERROR_INVALID_LOGIN = 2;
    public static final int ERROR_NO_USAGE = 3;
    public static final int ERROR_NO_INTERNET = 4;

    private static Ting ting;
    private String loginURL = "https://ting.com/account/login";
    private String accountURL = "https://ting.com/account";
    private String usageURL = "https://ting.com/json/account/get_account_usage_details";

    private String username;
    private String password;

    private DefaultHttpClient client;
    private HttpContext context;
    private CookieStore cookie;
    private HttpResponse response;
    private HttpResponse usageResponse;
    private String content;
    private JSONObject json;

    private SQLiteDatabase db;
    private TingDBHelper dbHelper;

    private TingUsage[] tingUsage = new TingUsage[3];
    private ArrayList<TingPlan>[] tingPlan = new ArrayList[3];
    private ArrayList<TingDevice> tingDevices = new ArrayList<TingDevice>();

    private String daysLeft = "Unknown";
    private boolean skipCache = false;
	private boolean[] parsed = {true, true, true};

    private int error = 0;

    private Ting(String username, String password) {
        BasicHttpParams params = new BasicHttpParams();
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        final SSLSocketFactory sslSocketFactory = SSLSocketFactory.getSocketFactory();
        schemeRegistry.register(new Scheme("https", sslSocketFactory, 443));
        ClientConnectionManager cm = new ThreadSafeClientConnManager(params, schemeRegistry);
        client = new DefaultHttpClient(cm, params);

        cookie = new BasicCookieStore();
        context = new BasicHttpContext();
        context.setAttribute(ClientContext.COOKIE_STORE, cookie);

        dbHelper = new TingDBHelper(TingApplication.getContext());
        db = dbHelper.getWritableDatabase();

        this.username = username;
        this.password = password;
    }

    static public Ting getInstance(String username, String password) {
        if (ting == null) {
            ting = new Ting(username, password);
        }

        return ting;
    }

    static public Ting getInstance() throws IllegalStateException {
        return getInstance(null, null);
    }

    public int getError() {
        return error;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private void setDashboardV2() {
        BasicClientCookie preview = new BasicClientCookie("preview", "on");
        preview.setDomain("ting.com");
        preview.setPath("/");
        cookie.addCookie(preview);
    }

    public boolean init(boolean refresh) {
        tingUsage = new TingUsage[3];
        tingPlan = new ArrayList[3];
        tingDevices = new ArrayList<TingDevice>();

        // Try to pull from the cache first
        if (!refresh && isReady()) {
            return true;
        }

        if (!isLoggedIn() || refresh) {
            cookie.clear();
            setDashboardV2();
            json = null;
            response = null;

            login();
        }

        return isLoggedIn();
    }

    public boolean isReady() {
        if (json == null) {
            loadCache();
        }

        return json != null;
    }

    private boolean login() {
        Log.d(TAG, "Trying to login");
        // Create a new Post Header
        HttpPost request = new HttpPost(loginURL);

        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("email", username));
            nameValuePairs.add(new BasicNameValuePair("password", password));
            nameValuePairs.add(new BasicNameValuePair("rememberme", "1"));
            nameValuePairs.add(new BasicNameValuePair("existing_user_login", ""));
            request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            response = client.execute(request, context);
        } catch (Exception e) {
            return getAccount();
        }

        if (isLoggedIn()) {
            return getAccount();
        } else {
            return false;
        }
    }

    public boolean getAccount() {
        HttpGet request;
        try {
            if (response == null) {
                Log.d(TAG, "Getting account details");
                request = new HttpGet(accountURL);
                response = client.execute(request, context);
            }

            if (usageResponse == null) {
                Log.d(TAG, "Getting usage details");
                request = new HttpGet(usageURL);
                usageResponse = client.execute(request, context);
            }

        } catch (Exception e) {
            error = ERROR_NO_INTERNET;
            return false;
        }

        return true;
    }

    public boolean processAccount() {
        try {
            content = inputStreamToString(response.getEntity().getContent());
            response.getEntity().consumeContent();

            Pattern pattern = Pattern.compile("(\\d+) days");
            Matcher matcher = pattern.matcher(content);
            if (matcher.find()) {
                daysLeft = matcher.group(0);
            }

            ContentValues updated = new ContentValues();
            updated.put(TingDBHelper.U_TYPE, TingDBHelper.TYPE_USAGE);
            updated.put(TingDBHelper.U_UPDATED, DateFormat.getDateTimeInstance().format(new Date()));
            db.insertWithOnConflict(TingDBHelper.TABLE_UPDATED, null, updated, SQLiteDatabase.CONFLICT_REPLACE);
        } catch (Exception e) {
            error = ERROR_NO_INTERNET;
            return false;
        }

        return true;
    }

    public boolean parseDashboard() {
        try {
            String jsonString = inputStreamToString(usageResponse.getEntity().getContent());
            usageResponse.getEntity().consumeContent();

            JSONObject json = new JSONObject();
            json.put("usage", new JSONObject(jsonString));
            json.put("daysLeft", daysLeft);

            if (content.length() > 0) {
                Log.d(TAG, "Processing account details");
                String cleanContent = content.toString();

                // Strip out everything except what we want
                cleanContent = cleanContent.substring(cleanContent.indexOf("var plans"));
                cleanContent = cleanContent.substring(4, cleanContent.indexOf(");")+1);

                Pattern find = Pattern.compile("\\$.\\(");
                Matcher match = find.matcher(cleanContent);
                cleanContent = match.replaceAll("");

                // No closing parentheses. May have to polish this later.
                find = Pattern.compile("\\)");
                match = find.matcher(cleanContent);
                cleanContent = match.replaceAll("");

                // Pull out graph containers as I don't need them and they break
                find = Pattern.compile("graphContainers .*?\\},");
                match = find.matcher(cleanContent);
                cleanContent = match.replaceAll("");

                // Strip out description for galaxy s2
                find = Pattern.compile("\\\"desc\\\" : \\\".*?\\\",");
                match = find.matcher(cleanContent);
                cleanContent = match.replaceAll("");

                find = Pattern.compile("(\\w*) = ([\\[|\\{])");
                match = find.matcher(cleanContent);
                cleanContent = match.replaceAll("\"$1\" : $2");

                // Wrap in braces to make legal
                cleanContent = "{" + cleanContent + "}";
                json.put("dashboard", new JSONObject(cleanContent));
            }

            Context tContext = TingApplication.getContext();
            FileOutputStream cache = new FileOutputStream(new File(tContext.getCacheDir(), "dashboard.json"));
            cache.write(json.toString().getBytes());
            cache.close();
            skipCache = false;

	        parsed[TYPE_MINUTES] = false;
	        parsed[TYPE_MESSAGES] = false;
	        parsed[TYPE_MEGABYTES] = false;
        } catch (JSONException e) {
            Log.d(TAG, "Problem parsing account page");
            error = ERROR_NOT_ACTIVATED;
            return false;
        } catch (Exception e) {
                return false;
        }

        return true;
    }

    public void loadCache() {
        if(skipCache) {
            return;
        }

        try {
            Context tContext = TingApplication.getContext();
            File cacheDir = tContext.getCacheDir();
            FileInputStream fis = new FileInputStream(new File(cacheDir, "dashboard.json"));
            StringBuffer fileContent = new StringBuffer("");

            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) != -1) {
                fileContent.append(new String(buffer));
            }

            json = new JSONObject(fileContent.toString());
        } catch (Exception e) {
        }
    }

    public Cursor getUsageDetail(int type) {
        return getUsageDetail(type, false, -1);
    }

    public Cursor getUsageDetail(int type, boolean invert) {
        return getUsageDetail(type, invert, -1);
    }

    private String getDeviceUsageDetailQuery(int type, boolean invert, int device) {
        String direction = "ASC";
        if (invert) {
            direction = "DESC";
        }

        // Check the database for existing entries of this type
        String query = "SELECT * FROM " + TingDBHelper.TABLE_USAGE + " WHERE TYPE = " + type;
        String order = " ORDER BY " + TingDBHelper.N_DATE + " " + direction + ", " + TingDBHelper.N_TIME + " " + direction;

        if (device != -1) {
            query += " AND " + TingDBHelper.N_DEVICE + " = " + device;
        }

        query += order;

        return query;
    }

    public Cursor getUsageDetail(int type, boolean invert, int device) {
        String query = getDeviceUsageDetailQuery(type, invert, device);
        Cursor results = db.rawQuery(query, null);

        if (results.getCount() == 0) {
            error = ERROR_NO_USAGE;
        }

        return results;
    }

    public void parseUsageDetail(int type) {
	    // If we've already parsed this data, don't do it again.
		if(parsed[type]) {
			return;
		}

        try {
            JSONArray usageArr;
            JSONObject usage;
            TingUsageDetail detail;

	        clearUsage(type);

            if (type == TYPE_MINUTES) {
                usageArr = json.getJSONObject("usage").getJSONArray("call_usage");
            } else if (type == TYPE_MESSAGES) {
                usageArr = json.getJSONObject("usage").getJSONArray("sms_usage");
            } else {
                usageArr = json.getJSONObject("usage").getJSONArray("data_usage");
            }

            for (int i = 0; i < usageArr.length(); i++) {
                detail = new TingUsageDetail();
                usage = usageArr.getJSONObject(i);

                switch (type) {
                    case TYPE_MINUTES:
                        detail.duration = usage.getString("call_duration");
                        if(usage.getInt("incoming") == 1) {
                            detail.setFrom(usage.getString("caller_id"));
                            detail.setTo(usage.getString("mdn"));
                        } else {
                            detail.setTo(usage.getString("called_number"));
                            detail.setFrom(usage.getString("mdn"));
                        }
                        detail.time = usage.getString("time");
                        detail.charges = usage.getString("extra_charges");
                        detail.setDate(usage.getString("date"));
                        break;
                    case TYPE_MESSAGES:
                        if(usage.getInt("sent") == 1) {
                            detail.setFrom(usage.getString("mdn"));
                            detail.setTo(usage.getString("partner"));
                        } else {
                            detail.setTo(usage.getString("mdn"));
                            detail.setFrom(usage.getString("partner"));
                        }
                        detail.time = usage.getString("time");
                        detail.charges = usage.getString("extra_charges");
                        detail.setDate(usage.getString("date"));
                        break;
                    case TYPE_MEGABYTES:
                        NumberFormat nf = NumberFormat.getInstance();
                        nf.setMaximumFractionDigits(2);
                        detail.usage = nf.parse("" + usage.getDouble("kilobytes")/1024).toString();
                        detail.setFrom(usage.getString("mdn"));
                        detail.charges = usage.getString("extra_charges");
                        detail.setDate(usage.getString("date"));
                        break;
                }

                db.insert("usage", null, detail.getContentValues(type));
            }

	        parsed[type] = true;
        } catch (Exception e) {
        }
    }

    public boolean isLoggedIn() {
        boolean loggedIn = false;

        // Check for successful login
        for (Cookie cookieCheck : cookie.getCookies()) {
            if (cookieCheck.getName().equals("tingweb_user_logged_in") && cookieCheck.getValue().equals("1")) {
                loggedIn = true;
            }
        }

        if (!loggedIn) {
            error = ERROR_INVALID_LOGIN;
        }

        return loggedIn;
    }

    public TingUsage[] getAllUsage() {
        TingUsage[] usage = new TingUsage[3];
        usage[0] = getUsage(TYPE_MINUTES);
        usage[1] = getUsage(TYPE_MESSAGES);
        usage[2] = getUsage(TYPE_MEGABYTES);

        return usage;
    }

    public ArrayList<TingPlan>[] getAllPlans() {
        ArrayList<TingPlan>[] plans = new ArrayList[3];
        plans[0] = getPlans(TYPE_MINUTES);
        plans[1] = getPlans(TYPE_MESSAGES);
        plans[2] = getPlans(TYPE_MEGABYTES);

        return plans;
    }

    public String getDaysLeft() {
        if (daysLeft.equals("Unknown") && json != null) {
            try {
                daysLeft = json.getString("daysLeft");
            } catch (JSONException e) {
            }
        }

        return daysLeft;
    }

    public String getLastUpdated(String type) {
        // Return the overall last updated time as everything is from cache
        type = TingDBHelper.TYPE_USAGE;

        String query = "SELECT " + TingDBHelper.U_UPDATED + " FROM " + TingDBHelper.TABLE_UPDATED +
                " WHERE " + TingDBHelper.U_TYPE + " = '" + type + "'";

        String updated = "Unknown";
        try {
            updated = DatabaseUtils.stringForQuery(db, query, null);
        } catch (Exception e) {
        }

        return updated;
    }

    public boolean hasUsage(int type) {
        return hasUsage(type, -1);
    }

    public boolean hasUsage(int type, int device) {
        String query = "SELECT count(*) FROM " + TingDBHelper.TABLE_USAGE + " WHERE type = " + type;

        if (device != -1) {
            query += " AND " + TingDBHelper.N_DEVICE + " = " + device;
        }

        String result = DatabaseUtils.stringForQuery(db, query, null);
        int count = Integer.parseInt(result);
        return count > 0;
    }

    public TingDevice getDevice(String device) {
        // If checking a phone number go through the list and check mdns
        if (PhoneNumberUtils.isGlobalPhoneNumber(device)) {
            device = device.replace("-", "");
        }

        for (TingDevice d : getDevices()) {
            try {
                if (d.phone.equals(device) || d.nick.equals(device) || d.id == Integer.parseInt(device)) {
                    return d;
                }
            } catch (Exception e) {
            }
        }

        return null;
    }

    private TingUsage getUsage(int type) {
        if (!(tingUsage[type] instanceof TingUsage)) {
            TingUsage usage = new TingUsage();

            try {
                JSONObject jsonUsage = json.getJSONObject("dashboard").getJSONObject("usage").getJSONObject(getType(type));
                usage.average = jsonUsage.getInt("average");
                usage.current = jsonUsage.getInt("current");
                usage.estimated = jsonUsage.getInt("estimated");
                usage.maximum = jsonUsage.getInt("maximum");

            } catch (Exception e) {
                e.printStackTrace();
            }

            tingUsage[type] = usage;
        }

        return tingUsage[type];
    }

    private ArrayList<TingPlan> getPlans(int type) {
        if (!(tingPlan[type] instanceof ArrayList)) {
            ArrayList<TingPlan> plans = new ArrayList<TingPlan>();

            try {
                JSONArray jsonPlans = json.getJSONObject("dashboard").getJSONObject("plans").getJSONObject(getType(type)).getJSONArray("buckets");
                JSONObject jsonPlan;
                for (int i = 0; i < jsonPlans.length(); i++) {
                    jsonPlan = jsonPlans.getJSONObject(i);
                    TingPlan plan = new TingPlan();
                    plan.cost = jsonPlan.getInt("cost");
                    plan.units = jsonPlan.getInt("units");
                    plan.size = jsonPlan.getString("size");
                    plans.add(plan);
                }
            } catch (Exception e) {
                error = ERROR_NOT_ACTIVATED;
            }

            tingPlan[type] = plans;
        }

        return tingPlan[type];
    }

    public ArrayList<TingDevice> getDevices() {
        if (tingDevices.size() == 0) {
            parseDevices();
        }

        return tingDevices;
    }

    public void parseDevices() {
        TingDevice device;

        if (!isReady()) {
            return;
        }

        try {
            JSONArray devices = json.getJSONObject("usage").getJSONArray("devices_with_stats");
            for (int i = 0; i < devices.length(); i++) {
                device = new TingDevice();
                JSONObject jsonDevice = (JSONObject) devices.get(i);

                device.id = jsonDevice.getInt("device_id");
                device.nick = jsonDevice.getString("nickname");
                device.phone = jsonDevice.getString("mdn");
                device.minutes.current = jsonDevice.getJSONObject("minutes").getInt("current");
                device.messages.current = jsonDevice.getJSONObject("messages").getInt("current");
                device.megabytes.current = jsonDevice.getJSONObject("megabytes").getInt("current");

                tingDevices.add(device);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void clearUsage(int type) {
        db.delete(TingDBHelper.TABLE_USAGE, TingDBHelper.N_TYPE + " = " + type, null);
    }

    public String getType(int type) {
        switch (type) {
            case TYPE_MINUTES:
                return "minutes";
            case TYPE_MESSAGES:
                return "messages";
            case TYPE_MEGABYTES:
                return "megabytes";
        }

        return null;
    }

    public void skipCache() {
        skipCache = true;
    }

    private String inputStreamToString(InputStream is) {
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder result = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
        }

        return result.toString();
    }

}