package net.ogd3.ting;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import net.ogd3.ting.activities.TingPreferenceActivity;
import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(
		formKey = "dG1Rb2c5SzRveGl5UVhINVJNcHMyRVE6MQ",
		sendReportsInDevMode = false,
        customReportContent = {
		        ReportField.REPORT_ID,
		        ReportField.APP_VERSION_CODE,
		        ReportField.CUSTOM_DATA,
		        ReportField.STACK_TRACE,
		        ReportField.ANDROID_VERSION,
		        ReportField.PHONE_MODEL,
		        ReportField.BRAND,
		        ReportField.PRODUCT,
		        ReportField.BUILD,
		        ReportField.TOTAL_MEM_SIZE,
		        ReportField.AVAILABLE_MEM_SIZE,
		        ReportField.INITIAL_CONFIGURATION,
		        ReportField.CRASH_CONFIGURATION,
		        ReportField.DISPLAY,
		        ReportField.INSTALLATION_ID,
		        ReportField.DEVICE_FEATURES,
		        ReportField.ENVIRONMENT
        }
)
public class TingApplication extends Application {
    private SharedPreferences sp;
    private static Context context;
	private boolean needFirstRun;

    @Override
    public void onCreate() {
        // The following line triggers the initialization of ACRA
        ACRA.init(this);
        super.onCreate();

        sp = PreferenceManager.getDefaultSharedPreferences(this);
        context = getApplicationContext();

        try {
            int versionNumber = context.getPackageManager().getPackageInfo(context.getPackageName(), 0 ).versionCode;
            int versionCheck = sp.getInt(TingPreferenceActivity.KEY_VERSION_NAME, 0);
            if(versionNumber != versionCheck) {
                Ting.getInstance().skipCache();
                sp.edit().putInt(TingPreferenceActivity.KEY_VERSION_NAME, versionNumber).commit();
            }
        } catch (PackageManager.NameNotFoundException e) {
        }


        needFirstRun = updateOnStart();
    }

    public static Context getContext() {
        return context;
    }

	public boolean needFirstRun() {
		boolean ret = needFirstRun;
		needFirstRun = false;

		return ret;
	}

	public boolean updateOnStart() {
		return sp.getBoolean(TingPreferenceActivity.KEY_UPDATE_START, false);
	}

    private boolean isDebuggable(Context ctx) {
        boolean debuggable = false;

        PackageManager pm = ctx.getPackageManager();
        try {
            ApplicationInfo appinfo = pm.getApplicationInfo(ctx.getPackageName(), 0);
            debuggable = (0 != (appinfo.flags &= ApplicationInfo.FLAG_DEBUGGABLE));
        } catch (PackageManager.NameNotFoundException e) {
            /*debuggable variable will remain false*/
        }

        return debuggable;
    }

}
