package net.ogd3.ting;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import net.ogd3.ting.containers.TingUsageDetail;

public class TingDBHelper extends SQLiteOpenHelper {
	private static final int VERSION = 5;
	private static final String DB_NAME = "TingDBHelper";

	public static final String TABLE_USAGE = "usage";

	public static final int C_ID = 0;
	public static final int C_DATE = 1;
	public static final int C_TIME = 2;
	public static final int C_FROM = 3;
	public static final int C_TO = 4;
	public static final int C_DURATION = 5;
	public static final int C_CHARGES = 6;
	public static final int C_USAGE = 7;
	public static final int C_TYPE = 8;
	public static final int C_DEVICE = 9;

	public static final String N_ID = "_id";
	public static final String N_DATE = "date";
	public static final String N_TIME = "time";
	public static final String N_FROM = "contactFrom";
	public static final String N_TO = "contactTo";
	public static final String N_DURATION = "duration";
	public static final String N_CHARGES = "charges";
	public static final String N_USAGE = "usage";
	public static final String N_TYPE = "type";
	public static final String N_DEVICE = "device";

	public static final String TABLE_UPDATED = "updated";

	public static final String U_TYPE = "type";
	public static final String U_UPDATED = "updated";

	public static final String TYPE_USAGE = "usage";
	public static final String TYPE_MINUTES = "minutes";
	public static final String TYPE_MEGABYTES = "megabytes";
	public static final String TYPE_MESSAGES = "messages";

	/*public static final String TABLE_DEVICES = "devices";

	public static final String D_ID = "_id";
	public static final String D_NICK = "nick";
	public static final String D_PHONE = "phone";
	public static final String D_MINUTES = "minutes";
	public static final String D_MESSAGES = "messages";
	public static final String D_MEGABYTES = "megabytes";*/

	public TingDBHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + TABLE_USAGE + " ( " +
				N_ID + " integer primary key autoincrement, " +
				N_DATE + " text, " +
				N_TIME + " text, " +
				N_FROM + " text, " +
				N_TO + " text, " +
				N_DURATION + " text, " +
				N_CHARGES + "'charges' text, " +
				N_USAGE + " text, " +
				N_TYPE + " numeric, " +
				N_DEVICE + " text " +
				");" +
				"CREATE INDEX usage_date_idx ON " + TABLE_USAGE + " (" + N_DATE + ");" +
				"CREATE INDEX usage_from_idx ON " + TABLE_USAGE + " (" + N_FROM + ");" +
				"CREATE INDEX usage_to_idx ON " + TABLE_USAGE + " (" + N_TO + ");" +
				"CREATE INDEX usage_device_idx ON " + TABLE_USAGE + " (" + N_DEVICE + ");" +
				"CREATE INDEX usage_type_idx ON " + TABLE_USAGE + " (" + N_TYPE + ");");

		db.execSQL("CREATE TABLE " + TABLE_UPDATED + " ( " +
				U_TYPE + " text primary key," +
				U_UPDATED + " text" +
				");");

		/*db.execSQL("CREATE TABLE " + TABLE_DEVICES + " ( " +
				D_ID + " integer primary key, " +
				D_NICK + " text, " +
				D_PHONE + " text, " +
				D_MINUTES + " integer, " +
				D_MESSAGES + " integer, " +
				D_MEGABYTES + " float " +
				");");*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USAGE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_UPDATED);
		//db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICES);
		onCreate(db);
	}

	public static TingUsageDetail cursorToDetail(Cursor c) {
		TingUsageDetail detail = new TingUsageDetail();

		detail.setDateFromDB(c.getString(C_DATE));
		detail.charges = c.getString(C_CHARGES);
		detail.duration = c.getString(C_DURATION);
		detail.usage = c.getString(C_USAGE);
		detail.time = c.getString(C_TIME);
		detail.setFrom(c.getString(C_FROM));
		detail.setTo(c.getString(C_TO));

		return detail;
	}
}
