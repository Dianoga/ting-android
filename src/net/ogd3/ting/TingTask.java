package net.ogd3.ting;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

public class TingTask extends AsyncTask<Void, Integer, Boolean> {
	private static final String TAG = "TingTask";

	public static final int DIALOG_UPDATE = 0;

	public static final int INIT = 100;

	private static final int PROGRESS_LOGIN = 0;
	private static final int PROGRESS_CHECK_DB = 1;
	private static final int PROGRESS_FETCH_TING = 2;
	private static final int PROGRESS_REFRESH = 3;
	private static final int PROGRESS_WORKING = 4;
	private static final int PROGRESS_PARSE_DASHBOARD = 5;
	private static final int PROGRESS_PARSE_MINUTES = 6;
	private static final int PROGRESS_PARSE_MESSAGES = 7;
	private static final int PROGRESS_PARSE_MEGABYTES = 8;
	private static final int PROGRESS_PARSE_USAGE = 9;

	private ProgressDialog dialog;
	private TingTaskListener activity;
	private boolean refresh;
	private boolean completed;
	private boolean notified;
	private boolean result;
	private int type;

	public TingTask(TingTaskListener activity, int type) {
		this(activity, type, false);
	}

	public TingTask(TingTaskListener activity, int type, boolean refresh) {
		attach(activity);
		this.type = type;
		this.refresh = refresh;
	}

	public boolean isComplete() {
		return completed;
	}

	@Override
	protected void onPreExecute() {
		activity.showDialog(DIALOG_UPDATE);
	}

	@Override
	protected Boolean doInBackground(Void... voids) {
		Ting ting = Ting.getInstance();
		boolean result = true;

		if (refresh || !ting.isReady()) {
			publishProgress(PROGRESS_FETCH_TING);
			if (ting.init(refresh)) {
				publishProgress(PROGRESS_WORKING);
				ting.processAccount();

				publishProgress(PROGRESS_PARSE_DASHBOARD);
				ting.parseDashboard();
				ting.parseDevices();

				/*
				publishProgress(PROGRESS_PARSE_MINUTES);
				ting.parseUsageDetail(Ting.TYPE_MINUTES);

				publishProgress(PROGRESS_PARSE_MESSAGES);
				ting.parseUsageDetail(Ting.TYPE_MESSAGES);

				publishProgress(PROGRESS_PARSE_MEGABYTES);
				ting.parseUsageDetail(Ting.TYPE_MEGABYTES);
				*/
			} else {
				return false;
			}
		}

		if (ting.isReady()) {
			Cursor results;
			int count;
			if (type != INIT) {
				publishProgress(PROGRESS_PARSE_USAGE);
				ting.parseUsageDetail(type);

				results = ting.getUsageDetail(type);
				count = results.getCount();
				results.close();
				result = count > 0;
			}
		}

		return result;
	}

	protected void onPostExecute(Boolean result) {
		completed = true;
		this.result = result;
		notifyTaskCompleted();
	}

	protected void onProgressUpdate(Integer... progress) {
		if (activity != null) {
			Bundle bundle = new Bundle();
			int message;

			switch (progress[0]) {
				case PROGRESS_LOGIN:
					message = R.string.progress_login;
					break;
				case PROGRESS_CHECK_DB:
					message = R.string.progress_check_db;
					break;
				case PROGRESS_FETCH_TING:
					message = R.string.progress_fetch_ting;
					break;
				case PROGRESS_REFRESH:
					message = R.string.progress_refresh;
					break;
				case PROGRESS_PARSE_DASHBOARD:
					message = R.string.progress_parse_dashboard;
					break;
				case PROGRESS_PARSE_MINUTES:
					message = R.string.progress_parse_minutes;
					break;
				case PROGRESS_PARSE_MESSAGES:
					message = R.string.progress_parse_messages;
					break;
				case PROGRESS_PARSE_MEGABYTES:
					message = R.string.progress_parse_megabytes;
					break;
				case PROGRESS_PARSE_USAGE:
					message = R.string.progress_parse_usage;
					break;
				default:
					message = R.string.progress_generic;
					break;
			}

			bundle.putInt("message", message);
			activity.showDialog(DIALOG_UPDATE, bundle);
		}
	}

	public void attach(TingTaskListener activity) {
		this.activity = activity;
		if (completed) {
			Log.d(TAG, "Activity attached and task complete");
			notifyTaskCompleted();
		} else {
			Log.d(TAG, "Activity attached and task not complete");
		}
	}

	public void detach() {
		this.activity = null;
	}

	private void notifyTaskCompleted() {
		if (activity != null) {
			Log.d(TAG, "Activity Notified");
			activity.onTingTaskCompleted(result);
			activity.removeDialog(DIALOG_UPDATE);
			notified = true;
		}
	}
}