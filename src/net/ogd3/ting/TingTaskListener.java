package net.ogd3.ting;

import android.os.Bundle;

public interface TingTaskListener {
	public void onTingTaskCompleted(boolean result);
	public boolean showDialog(int dialog, Bundle bundle);
	public void showDialog(int dialog);
	public void removeDialog(int dialog);
}
