package net.ogd3.ting.activities;

import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import net.ogd3.ting.*;
import net.ogd3.ting.adapters.TingDashboardListAdapter;
import net.ogd3.ting.containers.TingDevice;
import net.ogd3.ting.containers.TingUsage;

public class DashboardActivity extends ListActivity implements TingTaskListener {
	private static final String TAG = "DashboardActivity";

	private Ting ting;
	private SharedPreferences sp;
	private ProgressDialog dialog;
	private TingTask task;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		sp = PreferenceManager.getDefaultSharedPreferences(this);

		super.onCreate(savedInstanceState);

		setContentView(R.layout.list_activity);
	}

	@Override
	public void onStart() {
		super.onStart();

		String username = sp.getString(TingPreferenceActivity.KEY_USERNAME, null);
		String password = sp.getString(TingPreferenceActivity.KEY_PASSWORD, null);

		if (username == null || password == null) {
			Intent i = new Intent(this, TingPreferenceActivity.class);
			startActivity(i);
		} else {
			ting = Ting.getInstance(username, password);
			// Try to recover the previous task
			task = (TingTask) getLastNonConfigurationInstance();
			if(task != null && !task.isComplete()) {
				task.attach(this);
			} else {
				if (((TingApplication) getApplication()).needFirstRun() || !ting.isReady()) {
					if (task == null) {
						task = new TingTask(this, TingTask.INIT, true);
						task.execute();
					} else {
						task.attach(this);
					}
				} else {
					updateUI();
				}
			}
		}
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		if (task != null) {
			task.detach();
		}
		return task;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.options, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.optionPreferences:
				Intent i = new Intent(this, TingPreferenceActivity.class);
				startActivity(i);
				break;
			case R.id.optionRefresh:
				task = new TingTask(this, TingTask.INIT, true);
				task.execute();
				break;
			case R.id.optionAccount:
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ting.com/account"));
				startActivity(browserIntent);
				break;
		}
		return true;
	}

	public void onTingTaskCompleted(boolean result) {
		if (result) {
			updateUI();
		} else {
			setupEmpty();
			Toast.makeText(this, "Problem logging in", Toast.LENGTH_LONG).show();
		}
	}

	public void updateUI() {
		TingDashboardListAdapter la = new TingDashboardListAdapter(this);
		la.clear();
		la.setPlans(ting.getAllPlans());
		la.setNextBill(ting.getDaysLeft());
		for (TingUsage usage : ting.getAllUsage()) {
			la.add(usage);
		}

		for(TingDevice device : ting.getDevices()) {
			la.add(device);
		}

		setListAdapter(la);

		TextView updated = (TextView) findViewById(R.id.lastUpdated);
		updated.setText(ting.getLastUpdated(TingDBHelper.TYPE_USAGE));

		setupEmpty();
	}

	public void setupEmpty() {
		TextView empty = (TextView) findViewById(R.id.empty);
		if (getListAdapter() != null && getListAdapter().isEmpty()) {
			getListView().setVisibility(View.GONE);
			switch(ting.getError()) {
				case 1:
					empty.setText(R.string.error_not_activated);
					break;
				case 2:
					empty.setText(R.string.error_invalid_login);
					break;
				case 3:
					empty.setText(R.string.error_no_usage);
					break;
				case 4:
					empty.setText(R.string.error_no_internet);
					break;
				default:
					empty.setText(R.string.no_usage);
					break;
			}

			empty.setVisibility(View.VISIBLE);
		} else {
			getListView().setVisibility(View.VISIBLE);
			empty.setVisibility(View.GONE);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {
		switch (id) {
			case TingTask.DIALOG_UPDATE:
				ProgressDialog dialog = new ProgressDialog(DashboardActivity.this);
				dialog.setTitle("Working");
				((ProgressDialog) dialog).setIndeterminate(true);
				return dialog;
		}

		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog, Bundle args) {
		super.onPrepareDialog(id, dialog, args);

		String message = getString(R.string.progress_generic);
		if (args != null) {
			message = getString(args.getInt("message"));
		}

		((ProgressDialog) dialog).setMessage(message);
	}
}
