package net.ogd3.ting.activities;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import net.ogd3.ting.R;
import net.ogd3.ting.Ting;

public class TingPreferenceActivity extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";
	public static final String KEY_SORT_INVERT = "sort_invert";
	public static final String KEY_UPDATE_START = "update_start";
    public static final String KEY_VERSION_NAME = "version_name";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Settings");

        addPreferencesFromResource(R.xml.settings);
        PreferenceManager.getDefaultSharedPreferences(this).registerOnSharedPreferenceChangeListener(this);

        if (Build.VERSION.SDK_INT >= 11) {
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        Ting ting = Ting.getInstance();

        if (key.equals(KEY_PASSWORD)) {
            ting.setPassword(sp.getString(KEY_PASSWORD, null));
        } else if (key.equals(KEY_USERNAME)) {
            ting.setUsername(sp.getString(KEY_USERNAME, null));
        }
    }
}