package net.ogd3.ting.activities;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.*;
import android.widget.ListView;
import android.widget.TextView;
import net.ogd3.ting.R;
import net.ogd3.ting.Ting;
import net.ogd3.ting.TingTask;
import net.ogd3.ting.TingTaskListener;
import net.ogd3.ting.adapters.TingUsageDetailListAdapter;
import net.ogd3.ting.containers.TingDevice;

public class TingUsageActivity extends ListActivity implements TingTaskListener {
	private static final String TAG = "TingUsageActivity";

	private Ting ting;
	private SharedPreferences sp;
	private ProgressDialog dialog;
	private TingTask task;

	private boolean invert;
	private int type;
	private int device;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.list_activity);
		TextView empty = (TextView) findViewById(R.id.empty);
		empty.setText(R.string.no_usage_detail);

		type = getIntent().getExtras().getInt("type");
		device = getIntent().getExtras().getInt("device", -1);

		sp = PreferenceManager.getDefaultSharedPreferences(this);
		invert = sp.getBoolean(TingPreferenceActivity.KEY_SORT_INVERT, false);

		if (Build.VERSION.SDK_INT >= 11) {
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public void onStart() {
		super.onStart();

		ting = Ting.getInstance();

		String title = "Detailed Usage";
		switch (type) {
			case (Ting.TYPE_MINUTES):
				title = "Minutes";
				if (getListAdapter() == null) {
					getListView().addHeaderView(LayoutInflater.from(this).inflate(R.layout.list_usage_detail_header, null));
				}
				break;
			case (Ting.TYPE_MESSAGES):
				title = "Messages";
				if (getListAdapter() == null) {
					getListView().addHeaderView(LayoutInflater.from(this).inflate(R.layout.list_usage_detail_header, null));
				}
				break;
			case (Ting.TYPE_MEGABYTES):
				title = "Megabytes";
				break;
		}

		if (device != -1) {
			TingDevice d = ting.getDevice("" + device);
			if(d != null) {
				title = d.toString() + " - " + title;
			}
		}

		setTitle(title);
		setListAdapter(new TingUsageDetailListAdapter(this, type));

		task = (TingTask) getLastNonConfigurationInstance();
		if (task != null) {
			task.attach(this);
		} else {
			task = new TingTask(this, type);
			task.execute();
		}
	}

	@Override
	public void onStop() {
		try {
			((TingUsageDetailListAdapter) getListAdapter()).getCursor().close();
		} catch (Throwable t) {
		}
		super.onStop();
	}

	@Override
	public Object onRetainNonConfigurationInstance() {
		if (task != null) {
			task.detach();
		}
		return task;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.options, menu);

		menu.findItem(R.id.optionSort).setVisible(true);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.optionPreferences:
				Intent i = new Intent(this, TingPreferenceActivity.class);
				startActivity(i);
				break;
			case R.id.optionRefresh:
				task = new TingTask(this, type, true);
				task.execute();
				break;
			case R.id.optionSort:
				invert = !invert;
				updateUI(invert);
				break;
			case R.id.optionAccount:
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://ting.com/account"));
				startActivity(browserIntent);
				break;
			case android.R.id.home:
				Intent intent = new Intent(this, DashboardActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
				break;
		}
		return true;
	}

	public void onTingTaskCompleted(boolean result) {
		updateUI();
		setupEmpty();
	}

	public void updateUI() {
		updateUI(invert);
	}

	public void updateUI(boolean invert) {
		if (ting.hasUsage(type, device)) {
			TingUsageDetailListAdapter la = (TingUsageDetailListAdapter) getListAdapter();
			Cursor results = ting.getUsageDetail(type, invert, device);
			la.changeCursor(results);
		}

		TextView updated = (TextView) findViewById(R.id.lastUpdated);
		updated.setText(ting.getLastUpdated(ting.getType(type)));

		setupEmpty();
	}

	public void setupEmpty() {
		TextView empty = (TextView) findViewById(R.id.empty);
		ListView lv = getListView();
		if (getListAdapter().isEmpty()) {

			switch (ting.getError()) {
				case 1:
					empty.setText(R.string.error_not_activated);
					break;
				case 2:
					empty.setText(R.string.error_invalid_login);
					break;
				case 3:
					empty.setText(R.string.error_no_usage);
					break;
				case 4:
					empty.setText(R.string.error_no_internet);
					break;
				default:
					empty.setText(R.string.no_usage_detail);
					break;
			}
			lv.setVisibility(View.GONE);
			empty.setVisibility(View.VISIBLE);
		} else {
			lv.setVisibility(View.VISIBLE);
			empty.setVisibility(View.GONE);
		}
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle args) {
		switch (id) {
			case TingTask.DIALOG_UPDATE:
				ProgressDialog dialog = new ProgressDialog(TingUsageActivity.this);
				dialog.setTitle("Working");
				((ProgressDialog) dialog).setIndeterminate(true);
				return dialog;
		}

		return null;
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog, Bundle args) {
		super.onPrepareDialog(id, dialog, args);

		String message = getString(R.string.progress_generic);
		if (args != null && args.containsKey("message")) {
			message = getString(args.getInt("message"));
		}

		((ProgressDialog) dialog).setMessage(message);
	}
}