package net.ogd3.ting.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import net.ogd3.ting.R;
import net.ogd3.ting.Ting;
import net.ogd3.ting.TingProgressBar;
import net.ogd3.ting.activities.TingUsageActivity;
import net.ogd3.ting.containers.TingDevice;
import net.ogd3.ting.containers.TingPlan;
import net.ogd3.ting.containers.TingUsage;

import java.util.ArrayList;

public class TingDashboardListAdapter extends ArrayAdapter {
	private ArrayList<TingPlan>[] plans = new ArrayList[3];
	private String nextBill;

	public TingDashboardListAdapter(Context context) {
		super(context, 0);
	}

	@Override
	public View getView(int position, View convert, ViewGroup parent) {
		Object thing = getItem(position);
		View row;
		if (thing instanceof TingUsage) {
			row = createUsageView((TingUsage) thing, position);
		} else {
			row = createDeviceView((TingDevice) thing);
		}

		// Should we show the header?
		if (position == 0 || position == 3) {
			row.findViewById(R.id.typeHeader).setVisibility(View.VISIBLE);

			if(thing instanceof TingUsage) {
				TextView bill = (TextView) row.findViewById(R.id.typeBill);
				bill.setText("Next Bill: " + nextBill);
			}
		}

		return row;
	}

	public void setPlans(ArrayList<TingPlan>[] plans) {
		this.plans = plans;
	}

	public void setNextBill(String nextBill) {
		this.nextBill = nextBill;
	}

	private View createUsageView(TingUsage usage, final int position) {
		LayoutInflater li = LayoutInflater.from(getContext());
		View row = li.inflate(R.layout.list_usage, null);

		String[] titles = new String[]{"Minutes", "Messages", "Megabytes"};

		TingProgressBar progress = (TingProgressBar) row.findViewById(R.id.progress);
		TextView heading = (TextView) row.findViewById(R.id.heading);
		TextView units = (TextView) row.findViewById(R.id.planUnits);
		TextView size = (TextView) row.findViewById(R.id.planName);
		TextView estimated = (TextView) row.findViewById(R.id.estimated);
		TextView average = (TextView) row.findViewById(R.id.average);
		TextView max = (TextView) row.findViewById(R.id.max);
		TextView cost = (TextView) row.findViewById(R.id.planCost);

		ArrayList<TingPlan> plans = this.plans[position];

		heading.setText(titles[position]);
		average.setText("" + usage.average);
		max.setText("" + usage.maximum);
		if (usage.estimated > 0) {
			estimated.setText("" + usage.estimated);
		} else {
			row.findViewById(R.id.layoutEstimated).setVisibility(View.GONE);
		}

		String planCost = "$";
		String planUnits = "" + usage.current;
		progress.setText("" + planUnits);

		for (TingPlan plan : plans) {
			if (plan.units >= usage.current) {
				planCost += plan.cost;

				progress.setMax(plan.units);
				progress.setProgress(usage.current);
				units.setText("" + plan.units);
				size.setText(plan.size);
				cost.setText(planCost);
				break;
			}
		}

		row.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getContext(), TingUsageActivity.class);
				intent.putExtra("type", position);
				getContext().startActivity(intent);
			}
		});

		return row;
	}

	private View createDeviceView(final TingDevice device) {
		LayoutInflater li = LayoutInflater.from(getContext());
		View row = li.inflate(R.layout.list_device, null);

		TextView name = (TextView) row.findViewById(R.id.deviceName);
		TextView minutes = (TextView) row.findViewById(R.id.deviceMinutes);
		TextView messages = (TextView) row.findViewById(R.id.deviceMessages);
		TextView megabytes = (TextView) row.findViewById(R.id.deviceMegabytes);

		name.setText(device.toString());
		minutes.setText("" + device.minutes.current);
		messages.setText("" + device.messages.current);
		megabytes.setText("" + device.megabytes.current);

		minutes.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getContext(), TingUsageActivity.class);
				intent.putExtra("type", Ting.TYPE_MINUTES);
				intent.putExtra("device", device.id);
				getContext().startActivity(intent);
			}
		});

		messages.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getContext(), TingUsageActivity.class);
				intent.putExtra("type", Ting.TYPE_MESSAGES);
				intent.putExtra("device", device.id);
				getContext().startActivity(intent);
			}
		});

		megabytes.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(getContext(), TingUsageActivity.class);
				intent.putExtra("type", Ting.TYPE_MEGABYTES);
				intent.putExtra("device", device.id);
				getContext().startActivity(intent);
			}
		});

		return row;
	}

	public boolean isEnabled(int position) {
		return (getItem(position) instanceof TingUsage);
	}

	public boolean areAllItemsEnabled() {
		return false;
	}

}
