package net.ogd3.ting.adapters;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import net.ogd3.ting.*;
import net.ogd3.ting.containers.Contact;
import net.ogd3.ting.containers.TingDevice;
import net.ogd3.ting.containers.TingUsageDetail;

public class TingUsageDetailListAdapter extends CursorAdapter {
	private int type;
	private Ting ting;

	public TingUsageDetailListAdapter(Context context, int type) {
		super(context, null, false);
		Contact.setContext(context);
		this.type = type;

		ting = Ting.getInstance();
	}

	public View populateFrom(View row, Cursor c) {
		TingUsageDetail detail = TingDBHelper.cursorToDetail(c);

		// Setup the view parts that are the same for all usage types
		TextView date = (TextView) row.findViewById(R.id.usageDate);
		//TextView charges = (TextView) row.findViewById(R.id.usageCharges);
		date.setText(detail.getDate());

		/*if (!detail.charges.equals("")) {
			charges.setText(detail.charges);
			charges.setVisibility(View.VISIBLE);
		} else {
			charges.setVisibility(View.GONE);
		}*/

		// Here we break out the logic that is unique for different usage types
		if (type == Ting.TYPE_MINUTES || type == Ting.TYPE_MESSAGES) {
			TextView time = (TextView) row.findViewById(R.id.usageTime);
			TextView contactTextFrom = (TextView) row.findViewById(R.id.usageContactTextFrom);
			QuickContactBadge contactBadgeFrom = (QuickContactBadge) row.findViewById(R.id.usageContactBadgeFrom);
			TextView contactTextTo = (TextView) row.findViewById(R.id.usageContactTextTo);
			QuickContactBadge contactBadgeTo = (QuickContactBadge) row.findViewById(R.id.usageContactBadgeTo);
			TextView duration = (TextView) row.findViewById(R.id.usageDuration);

			time.setText(detail.time);

			if (detail.duration != null && !detail.duration.equals("")) {
				duration.setText(detail.duration);
				duration.setVisibility(View.VISIBLE);
			} else {
				duration.setVisibility(View.GONE);
			}

			// Contact information only exists for texts and calls

			String from = detail.getFrom();
			String to = detail.getTo();
			TingDevice device;

			// Setup from stuffs
			if((device = ting.getDevice(from)) != null) {
				from = device.phone;
			}

			if (PhoneNumberUtils.isGlobalPhoneNumber(from)) {
				contactBadgeFrom.setVisibility(View.VISIBLE);
				Contact contact = Contact.getContactByPhone(from);
				if (contact != null) {
					contactBadgeFrom.assignContactUri(contact.uri);
					contactBadgeFrom.setImageDrawable(contact.getPicture());
					contactTextFrom.setText(contact.name);
				} else {
					contactBadgeFrom.assignContactFromPhone(from, true);
					contactBadgeFrom.setImageResource(R.drawable.person);

					if(device != null) {
						contactTextFrom.setText(device.toString());
					} else {
						contactTextFrom.setText(from);
					}
				}

				contactBadgeFrom.setMode(ContactsContract.QuickContact.MODE_SMALL);
			} else {
				contactTextFrom.setText(from);
				contactBadgeFrom.setVisibility(View.INVISIBLE);
			}

			// Setup to stuffs
			if((device = ting.getDevice(to)) != null) {
				to = device.phone;
			}

			if (PhoneNumberUtils.isGlobalPhoneNumber(to)) {
				contactBadgeTo.setVisibility(View.VISIBLE);
				Contact contact = Contact.getContactByPhone(to);
				if (contact != null) {
					contactBadgeTo.assignContactUri(contact.uri);
					contactBadgeTo.setImageDrawable(contact.getPicture());
					contactTextTo.setText(contact.name);
				} else {
					contactBadgeTo.assignContactFromPhone(to, true);
					contactBadgeTo.setImageResource(R.drawable.person);

					if(device != null) {
						contactTextTo.setText(device.toString());
					} else {
						contactTextTo.setText(to);
					}
				}

				contactBadgeTo.setMode(ContactsContract.QuickContact.MODE_SMALL);
			} else {
				contactTextTo.setText(to);
				contactBadgeTo.setVisibility(View.INVISIBLE);
			}
		} else if (type == Ting.TYPE_MEGABYTES) {
			TextView usage = (TextView) row.findViewById(R.id.usageUsage);
			TextView device = (TextView) row.findViewById(R.id.usageDevice);

			usage.setText(detail.usage);
			device.setText(detail.getDevice().toString());
		}

		return row;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
		LayoutInflater li = LayoutInflater.from(context);
		int view = R.layout.list_usage_detail;
		if (type == Ting.TYPE_MEGABYTES) {
			view = R.layout.list_usage_detail_megabytes;
		}

		View row = li.inflate(view, null);

		return populateFrom(row, cursor);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		populateFrom(view, cursor);
	}


}
