package net.ogd3.ting.containers;


import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;

import java.util.HashMap;

public class Contact {
	public Uri uri;
	public String id;
	public String name;

	private static HashMap<String, Contact> contactMap = new HashMap<String, Contact>();
	private static Context context;

	public static void setContext(Context context) {
		Contact.context = context;
	}

	public static Contact getContactByPhone(String number) {
		Contact contact = contactMap.get(number);
		if (contact == null) {
			Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
			String[] projection = new String[]{
					ContactsContract.PhoneLookup.DISPLAY_NAME,
					ContactsContract.PhoneLookup._ID
			};
			Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
			if (cursor != null && cursor.moveToFirst()) {
				contact = new Contact();

				// Get values from contacts database:
				contact.id = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup._ID));
				contact.name = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));

				// Get photo of contactId as input stream:
				uri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contact.id));
				contact.uri = uri;
			}
			cursor.close();
			contactMap.put(number, contact);
		}

		return contact;
	}

	public Drawable getPicture() {
		return Drawable.createFromStream(ContactsContract.Contacts.openContactPhotoInputStream(context.getContentResolver(), uri), null);
	}
}

