package net.ogd3.ting.containers;

public class TingDevice {
	public int id;
	public String nick;
	public String phone;

	public TingUsage minutes;
	public TingUsage messages;
	public TingUsage megabytes;

	public TingDevice() {
		minutes = new TingUsage();
		messages = new TingUsage();
		megabytes = new TingUsage();
	}

	public String toString() {
		if (nick != null && !nick.equals("null")) {
			return nick;
		} else if(phone != null) {
			return phone.replace("(\\d{3})(\\d{3})(\\d{4})", "$1-$2-$3");
		} else {
			return "Unknown";
		}
	}
}