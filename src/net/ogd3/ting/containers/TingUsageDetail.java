package net.ogd3.ting.containers;

import android.content.ContentValues;
import net.ogd3.ting.Ting;
import net.ogd3.ting.TingDBHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TingUsageDetail {
	private Date date;
	public String time;
	private String from;
	private String to;
	public String duration;
	public String charges;
	public String usage;
	private String device;

	private Ting ting;

	public TingUsageDetail() {
		ting = Ting.getInstance();
	}

	public void setDate(String date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.date = df.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void setDateFromDB(String date) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.date = df.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public String getDate() {
		return new SimpleDateFormat("MMM dd").format(date);
	}

	public void setFrom(String number) {
		from = number;
	}

	public void setTo(String number) {
		to = number;
	}

	public String getFrom() {
		if (from != null) {
			return from.replaceAll(" \\(.*\\)$", "");
		} else {
			return null;
		}
	}

	public String getTo() {
		if (to != null) {
			return to.replaceAll(" \\(.*\\)$", "");
		} else {
			return null;
		}
	}

	public TingDevice getDevice() {
		TingDevice thinger;
		if ((thinger = ting.getDevice(getFrom())) != null) {
			return thinger;
		} else if ((thinger = ting.getDevice(getTo())) != null) {
			return thinger;
		} else {
			return new TingDevice();
		}
	}

	public ContentValues getContentValues(int type) {
		ContentValues cv = new ContentValues();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		TingDevice d = getDevice();

		cv.put(TingDBHelper.N_DATE, df.format(date));
		cv.put(TingDBHelper.N_TIME, time);
		cv.put(TingDBHelper.N_FROM, from);
		cv.put(TingDBHelper.N_TO, to);
		cv.put(TingDBHelper.N_DURATION, duration);
		cv.put(TingDBHelper.N_CHARGES, charges);
		cv.put(TingDBHelper.N_USAGE, usage);
		cv.put(TingDBHelper.N_TYPE, type);
		cv.put(TingDBHelper.N_DEVICE, getDevice().id);

		return cv;
	}
}
